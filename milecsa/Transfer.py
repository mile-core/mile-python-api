from __milecsa import __transfer_assets as transfer_assets
from milecsa import Wallet
from .Transaction import Transaction


class Transfer(Transaction):

    def __init__(self, src, dest, asset, amount, id="0"):

        Transaction.__init__(self, wallet=src, id=id)

        self.asset = int(asset)
        self.amount = str(amount)

        if type(dest) is Wallet.Wallet:
            self.destination = dest.publicKey
        else:
            self.destination = dest

        if type(src) is Wallet.Wallet:
            self.source = src.publicKey
        else:
            self.source = src

    def build(self):
        self.data = transfer_assets(self.wallet.publicKey,
                                    self.wallet.privateKey,
                                    self.destination,
                                    self.id,
                                    self.asset,
                                    self.amount)
