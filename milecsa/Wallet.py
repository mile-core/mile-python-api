from __milecsa import \
    __key_pair as key_pair, \
    __key_pair_with_secret_phrase as key_pair_with_secret_phrase, \
    __key_pair_from_private_key as key_pair_from_private_key

from milecsa.Rpc import Rpc
from milecsa.Chain import Chain
from milecsa.Transfer import Transfer
from milecsa.Shared import Shared


class Asset:

    def __init__(self, name, code):
        self.name = name
        self.code = code

    def __eq__(self, other):
        return self.code == other.code and self.name == other.name

    def __str__(self):
        return '{"%s":%s}' % (self.name, self.code)


class Balance:

    def __init__(self, asset, amount):
        self.asset = asset
        self.amount = amount

    def __eq__(self, other):
        return self.asset == other.asset and self.amount == other.amount

    def __str__(self):
        return '%s: %s' % (self.asset, self.amount)


class Wallet:

    class State:

        def __init__(self, balances, last_transaction_id):
            self.balances = balances
            self.last_transaction_id = last_transaction_id

        def get_balance(self, name=None, code=None):
            if name:
                x = [x for x in self.balances if x.name == name]
            elif code:
                x = [x for x in self.balances if x.code == code]
            return x

    publicKey = None
    privateKey = None

    def __init__(self, public_key=None, private_key=None, phrase=None, name=""):

        p = {}
        self.name = name
        self.phrase = phrase

        if public_key and private_key:
            p['public_key'] = public_key
            p['private_key'] = private_key

        elif private_key:
            p = key_pair_from_private_key(private_key)

        elif phrase:
            p = key_pair_with_secret_phrase(phrase)

        elif public_key:
            p['public_key'] = public_key
            p['private_key'] = None

        else:
            p = key_pair()

        self.publicKey = p['public_key']
        self.privateKey = p['private_key']

        self.__rpc_sate = Rpc("get-wallet-state", params={"public_key": self.publicKey})
        self.__chain = Chain()
        self.__shared = Shared()

    def __eq__(self, other):
        return (self.privateKey == other.privateKey) and (self.publicKey == other.publicKey)

    def get_state(self):
        response = self.__rpc_sate.exec()
        balances = []
        for b in response.result['balance']:
            asset_code = b['asset_code']
            asset_name = self.__chain.asset_name(code=asset_code)
            amount = b['amount']
            balances.append(Balance(asset=Asset(asset_name, asset_code), amount=amount))

        return Wallet.State(balances=balances,
                            last_transaction_id=response.result['last_transaction_id'])

    def get_transfers(self, limit=1000):
        rpc_transactions = Rpc("get-wallet-transactions", params={"public_key": self.publicKey, "count": limit})
        response = rpc_transactions.exec()

        trx_list = []
        for t in response.result['transactions']:
            d = t['description']
            if d['type'] == 'TransferAssetsTransaction':
                asset = d['assets']
                amount = d['amount']
                frm = d['from']
                to = d['to']
                _id = d['id']
                if type(asset) is str:
                    asset = self.__chain.asset_code(name=asset)
                trx = Transfer(src=frm, dest=to, asset=asset, amount=amount, id=_id)
                trx_list.append(trx)

        return trx_list


    def transfer(self, dest, asset, amount):

        if type(dest) is Wallet:
            destination = dest.publicKey
        elif type(dest) is type(""):
            destination = dest

        state = self.get_state()

        trx = Transfer(src=self, dest=destination, asset=asset, amount=amount, id=state.last_transaction_id)

        return trx.send()

    def payment_qr(self, asset, amount):
        return self.__shared.payment_qr(public_key=self.publicKey, asset=asset, amount=amount)

    def public_key_qr(self):
        return self.__shared.public_key_qr(public_key=self.publicKey, name=self.name)

    def private_key_qr(self):
        return self.__shared.private_key_qr(private_key=self.publicKey, name=self.name)

    def phrase_qr(self):
        if not self.phrase:
            return None
        return self.__shared.secret_phrase_qr(phrase=self.phrase)
