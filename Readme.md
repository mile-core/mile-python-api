# Requirements 

1. Python3
1. setuptools
1. git
1. gcc


# Tested on:

1. Ubuntu 17.04
1. OSX 10.13

# Build & Install

    $ git clone https://bitbucket.org/mile-core/mile-python-api
    $ cd mile-python-api
    $ git submodule update --init --recursive --remote
    $ python3 ./setup.py build
    $ sudo python3 ./setup.py install
    
#Test 

    $ python3 ./tests/module_test.py
     
     
#Examples

**Global configuration**
```python
from milecsa.Config import Config

#
# disable SSL verification
#
Config.sslVerification = False

#
# set up user defined connection timeout
#
Config.connectionTimeout = 30 # seconds

#
# use MILE testnet 
#
Config.url = "https://wallet.testnet.mile.global"

```

**Wallet create**
```python
from milecsa.Wallet import Wallet


def main():

    wallet0 = Wallet(phrase="Some phrase")

    print(wallet0.publicKey, wallet0.privateKey)

    #
    # Put your address
    #
    wallet1 = Wallet(public_key="...")

    state = wallet1.get_state()
    print(state.balances, state.last_transaction_id, wallet1.publicKey)
    for b in state.balances:
        print(b)

    #
    # Keep Secret phrase to qrcode
    #
    wqr0 = wallet0.phrase_qr()
    print(wqr0)
    wqr0.save("./img-wqr0.png")

    #
    # Generate Payment Ticket
    #
    wqr1 = wallet0.payment_qr(asset=1, amount=10)
    print(wqr1)
    wqr1.save("./img-wqr1.png")

if __name__ == '__main__':
    main()
```

**Send transfer**
```python
from milecsa import Transfer, Wallet
import time


def main():

    src = Wallet(phrase="Some WTF!? secret phrase")
    print(src.publicKey, src.privateKey)

    dst = Wallet()

    dst_public_key = Wallet().publicKey

    state = src.get_state()

    trx0 = Transfer(src=src, dest=dst, asset=1, amount=0.001, id=state.last_transaction_id)
    trx1 = Transfer(src=src, dest=dst_public_key, asset=1, amount=10, id=state.last_transaction_id)

    print(trx0.data)
    print(trx1.data)

    #
    # Put your address
    #
    src = Wallet(private_key="...")

    print(src.publicKey, src.privateKey)

    #
    # Put your address
    #
    dst = Wallet(public_key="...")

    result = src.transfer(dest=dst, asset=1, amount=1)

    print(result)

    time.sleep(60)

    state = dst.get_state()
    for b in state.balances:
        print(b)



if __name__ == "__main__":
    main()
```

**Manage Nodes**
```python
from milecsa import Wallet, Node

wallet = Wallet()
wallet1 = Wallet()

node0 = Node(wallet=wallet, address="https://my-mile.io")
node1 = Node(wallet=wallet1, address="https://mile.global")

print(node0.register())
print(node1.register())

print(node0.unregister())
print(node1.unregister())
```

**Send user transaction**
```python
from milecsa import Wallet, UserTransaction

wallet = Wallet()

data0 = UserTransaction(wallet=wallet, data='{k:"data",i:20,desc:"Some user can send transaction"}')
data1 = UserTransaction(wallet=wallet, data='{k:"data",i:20,desc:"Some another data"}', id="10")

print(data0.data, data0.userData)
print(data1.data, data1.userData)

```